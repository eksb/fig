package net.sbyrne.fig

import java.io.File

import org.parboiled.BaseParser
import org.parboiled.Parboiled
import org.parboiled.Rule
import org.parboiled.annotations.BuildParseTree
import org.parboiled.errors.ErrorUtils
import org.parboiled.matchers.Matcher
import org.parboiled.matchers.TestMatcher
import org.parboiled.parserunners.TracingParseRunner
import org.parboiled.support.ParseTreeUtils
import org.parboiled.support.StringVar

object FigParser {

	fun parse(file: File): FigValue = parse(file.readText())

	fun parse(text: String): FigValue {
		println("====== parse <<<${text}>>>")
		val parser = Parboiled.createParser(FigPegParser::class.java)
		//val runner = ReportingParseRunner<FigValue>(parser.FigRule())
		val runner = TracingParseRunner<Any?>(parser.FigRule())
		val result = runner.run(text)
		if (result.hasErrors()) {
			throw Exception(ErrorUtils.printParseError(result.parseErrors.first()))
		}
		println("-----[ ${text} ]-----")
		println(ParseTreeUtils.printNodeTree(result))
		println("--------")
		val value = result.valueStack.pop() as FigValue
		if ( ! result.valueStack.isEmpty ) throw Exception( "Unexpected stuff on stack: ${result.valueStack.joinToString(",")}" )
		return value
	}

}

@BuildParseTree
open class FigPegParser: BaseParser<Any>() {

	companion object {
		const val WHITESPACE =
				"\u0009" + // CHARACTER TABULATION (\t)
				"\u000A" + // LINE FEED (\n)
				"\u000B" + // LINE TABULATION
				"\u000C" + // FORM FEED
				"\u000D" + // CARRIAGE RETURN (\r)
				"\u001C" + // INFORMATION SEPARATOR FOUR
				"\u001D" + // INFORMATION SEPARATOR THREE
				"\u001E" + // INFORMATION SEPARATOR TWO
				"\u001F" + // INFORMATION SEPARATOR ONE
				"\u0020" + // SPACE
				"\u00A0" + // NO-BREAK SPACE
				"\u1680" + // OGHAM SPACE MARK
				"\u2000" + // EN QUAD
				"\u2001" + // EM QUAD
				"\u2002" + // EN SPACE
				"\u2003" + // EM SPACE
				"\u2004" + // THREE-PER-EM SPACE
				"\u2005" + // FOUR-PER-EM SPACE
				"\u2006" + // SIX-PER-EM SPACE
				"\u2007" + // FIGURE SPACE
				"\u2008" + // PUNCTUATION SPACE
				"\u2009" + // THIN SPACE
				"\u200A" + // HAIR SPACE
				"\u202F" + // NARROW NO-BREAK SPACE
				"\u205F" + // MEDIUM MATHEMATICAL SPACE
				"\u3000" + // IDEOGRAPHIC SPACE
				"\u2028" + // LINE SEPARATOR 	 
				"\u2029"   // PARAGRAPH SEPARATOR
	}

	// pushes FigList or FigMap to stack
	open fun FigRule(): Rule {
		return FirstOf(
				Sequence(
						WhitespaceRule(),
						FigListRule(), // pushes FigList
						WhitespaceRule(),
						EOI.skipNode()
				),
				Sequence(
						WhitespaceRule(),
						FigMapRule(), // pushes FigMap
						WhitespaceRule(),
						EOI.skipNode()
				),
				Sequence(
						WhitespaceRule(),
						ImpliedFigListRule(), // pushes FigList
						WhitespaceRule(),
						EOI.skipNode()
				)
		)
	}

	open fun WhitespaceTerminatorOrEndRule(terminators:String?): Rule {
		return FirstOf(
				AnyOf("${terminators?:""}${WHITESPACE}"),
				CommentRule(),
				EOI
		)
	}

	open fun CommentRule(): Rule {
		return Sequence(
				"<",
				ZeroOrMore(
						Sequence(
								TestNot( ">" ),
								ANY
						)
				),
				">"
		)
	}

	open fun TestWhitespaceTerminatorOrEnd(terminators:String?): Matcher {
		return TestMatcher(
				WhitespaceTerminatorOrEndRule(terminators)
		)
	}

	open fun WhitespaceRule(): Rule {
		return ZeroOrMore(
				FirstOf(
						AnyOf(WHITESPACE),
						CommentRule()
				)
		).suppressNode()
	}

	// pushes FigList
	open fun FigListRule(): Rule {
		return Sequence(
				AnyOf("[").skipNode(),
				FigListContentRule("]"), // pushes FigList
				FirstOf(
						AnyOf("]"),
						EOI
				).suppressNode()
		).skipNode()
	}

	// pushes FigList
	open fun ImpliedFigListRule(): Rule {
		return FigListContentRule(null) // pushes FigList
	}

	// pushes FigList
	open fun FigListContentRule(terminators:String?): Rule {
		return Sequence(
				push(FigList()),
				ZeroOrMore(
						Sequence(
								WhitespaceRule(),
								ValueRule(terminators), // pushes FigValue?
								addFigListValue(), // adds FigValue? on top of stack to FigList at next position on stack
								WhitespaceRule()
						).skipNode()
				)
		)
	}

	open fun addFigListValue(): Boolean {
		( peek(1) as FigList ).list.add( pop() as FigValue? )
		return true
	}

	// pushes FigMap
	open fun FigMapRule(): Rule {
		return Sequence(
				String("{").skipNode(),
				FirstOf(
						Sequence(
								FigMapIdentifierSegmentRule(), // pushes String
								push(FigMap(name=pop() as String))
						),
						push(FigMap())
				).skipNode(),
				FigMapContentRule(), // adds k/v to map on stack
				FirstOf(
						String("}"),
						EOI
				).suppressNode()
		)
	}

	// pushes String
	open fun FigMapIdentifierSegmentRule(): Rule {
		return Sequence(
				String("%").skipNode(),
				FigMapIdentifierRule(), // pushes String
				TestWhitespaceTerminatorOrEnd("}")
		).skipNode()
	}

	// pushes String
	open fun FigMapIdentifierRule(): Rule {
		return Sequence(
				ZeroOrMore(
						Sequence(
								TestNot(
									WhitespaceTerminatorOrEndRule("}")
							),
							ANY
						)
				),
				push(match())
		).suppressSubnodes()
	}

	// adds k/v's to FigMap on stack
	open fun FigMapContentRule(): Rule {
		return ZeroOrMore(
				Sequence(
						WhitespaceRule(),
						FigMapKeyValueRule(), // adds k/v to FigMap on stack
						TestWhitespaceTerminatorOrEnd("}"),
						WhitespaceRule()
				).skipNode()
		).skipNode()
	}

	// adds k/v to FigMap on stack
	open fun FigMapKeyValueRule(): Rule {
		return Sequence(
				FirstOf(
						FigMapKeyWithValueRule(),
						FigMapValueWithoutKeyRule(),
						FigMapKeyWithoutValueRule()
				),
				addEntryToMap()
		).skipNode()
	}

	data class FigMapKey(val key:FigString?)
	data class FigMapValue(val value:FigValue?)

	open fun addEntryToMap(): Boolean {
		var key:FigMapKey? = null
		var value:FigMapValue? = null
		while ( peek() !is FigMap ) {
			val obj = pop()
			when ( obj ) {
				is FigMapKey -> key = obj
				is FigMapValue -> value = obj
				else -> throw Exception("Unexpected ${obj}" )
			}
		}
		( peek() as FigMap ).map[key?.key?.value] = value?.value
		return true
	}

	open fun FigMapKeyWithValueRule(): Rule {
		return Sequence(
				StringLiteralRule(":}"),
				push( FigMapKey( pop() as FigString ) ),
				WhitespaceRule(),
				String(":").suppressNode(),
				WhitespaceRule(),
				Optional(
						Sequence(
								ValueRule("}"),
								push( FigMapValue( pop() as FigValue? ) )
						)
				).skipNode()
		)
	}

	open fun FigMapValueWithoutKeyRule(): Rule {
		return Sequence(
				WhitespaceRule(),
				String(":").suppressNode(),
				WhitespaceRule(),
				ValueRule("}"),
				push( FigMapValue( pop() as FigValue? ) )
		)
	}

	open fun FigMapKeyWithoutValueRule(): Rule {
		return Sequence(
				StringLiteralRule(":}"),
				push( FigMapKey( pop() as FigString ) )
		)
	}

	// pushes FigValue? to stack
	open fun ValueRule(terminators:String?): Rule {
		return FirstOf(
						NullLiteralRule(terminators), // pushes null to stack
						BooleanLiteralRule(terminators), // pushes FigBoolean to stack
						NumericLiteralRule(terminators), // pushes FigNumber to stack
						FigListRule(), // pushes FigList to stack
						FigMapRule(), // pushes FigMap to stack
						StringLiteralRule(terminators) // pushes FigString to map
				).skipNode()
	}

	// pushes null
	open fun NullLiteralRule(terminators:String?): Rule {
		return Sequence(
				String( "null" ),
				TestWhitespaceTerminatorOrEnd(terminators),
				push(null)
		).suppressSubnodes()
	}

	// pushes FigBoolean
	open fun BooleanLiteralRule(terminators:String?): Rule {
		return Sequence(
				FirstOf(
						Sequence(
								"true",
								push( FigBoolean.TRUE )
						),
						Sequence(
								"false",
								push( FigBoolean.FALSE )
						)
				),
				TestWhitespaceTerminatorOrEnd(terminators)
		).suppressSubnodes()
	}

	// pushes FigNumber
	open fun NumericLiteralRule(terminators:String?): Rule {
		return Sequence(
				Sequence(
						Optional(
								AnyOf( "+-" )
						),
						OneOrMore(DigitRule()),
						Optional(
								Sequence(
										".",
										OneOrMore(DigitRule())
								)
						),
						Optional(
								"E",
								Optional(
										AnyOf("+-")
								),
								OneOrMore(
										DigitRule()
								)
						),
						TestWhitespaceTerminatorOrEnd(terminators)
				),
				push( FigNumber(match() ) )
		).suppressSubnodes()
	}

	open fun DigitRule(): Rule {
		return AnyOf( "0123456789" )
	}

	// pushes FigString
	open fun StringLiteralRule( terminators:String? ): Rule {
		return FirstOf(
				QuotedStringRule(),
				UnquotedStringRule(terminators)
		).suppressSubnodes()
	}

	// pushes FigString
	open fun QuotedStringRule(): Rule {
		val text = StringVar()
		return Sequence(
				"\"",
				TextOrEmptyRule(charsRequiringEscape = "\"",var_=text),
				push( FigString( text.get() ) ),
				"\""
		)
	}

	// pushes FigString
	open fun UnquotedStringRule(terminators:String?): Rule {
		val text = StringVar()
		return Sequence(
				TextRule(charsRequiringEscape = "${WHITESPACE}${terminators?:""}",var_=text),
				push( FigString( text.get() ) ),
				TestWhitespaceTerminatorOrEnd(terminators)
		)
	}

	/**
	 * A text string where the specified characters and backslash must be escaped with a backslash.
	 * The text can be empty.
	 *
	 * @param charsRequiringEscape
	 */
	open fun TextOrEmptyRule(charsRequiringEscape: String,var_:StringVar): Rule {
		return ZeroOrMore(
				FirstOf(
						Sequence(
								'\\',
								AnyOf(charsRequiringEscape + '\\'),
								var_.append(match())
						),
						OneOrMore(
								Sequence(
										TestNot(
												AnyOf(charsRequiringEscape + '\\')
										),
										ANY,
										var_.append(match())
								)
						).suppressSubnodes()
				)
		)
	}

	/**
	 * A text string where the specified characters and backslash must be escaped with a backslash.
	 * The text can be empty.
	 *
	 * @param charsRequiringEscape
	 */
	open fun TextRule(charsRequiringEscape: String,var_:StringVar): Rule {
		return OneOrMore(
				FirstOf(
						Sequence(
								'\\',
								AnyOf(charsRequiringEscape + '\\'),
								var_.append(match())
						),
						OneOrMore(
								Sequence(
										TestNot(AnyOf(charsRequiringEscape + '\\')),
										ANY,
										var_.append(match())
								)
						).suppressSubnodes()
				)
		)
	}

}
