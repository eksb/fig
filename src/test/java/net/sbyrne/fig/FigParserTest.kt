package net.sbyrne.fig

import java.math.BigDecimal
import kotlin.test.*

class FigParserTest {

	fun test(input:String,ref:FigValue?=null) {
		val result = FigParser.parse(input)
		println( result )
		if ( ref != null ) {
			assertEquals(ref,result)
		}
	}

	@Test
	fun listOfNull() = test(
			"[null]",
			FigList(mutableListOf(null))
	)

	@Test
	fun listOfNullSpaced() = test(
			"[ null ]",
			FigList(mutableListOf(null))
	)

	@Test
	fun listOfNullAndBooleans() = test(
			" [null true false null] ",
			FigList(mutableListOf(
					null,
					FigBoolean(true),
					FigBoolean(false),
					null
			))
	)

	@Test
	fun implicitListOfNull() = test(
			"null",
			FigList(mutableListOf(null))
	)

	@Test
	fun implicitListofNullSpaced() = test(
			" null ",
			FigList(mutableListOf(null))
	)

	@Test
	fun implicitListOfNullAndBooleans() = test(
			" null true false null ",
			FigList(mutableListOf(
					null,
					FigBoolean(true),
					FigBoolean(false),
					null
			) )
	)

	@Test
	fun stringStartingWithNumber() = test(
			"5a",
			FigList(mutableListOf(
					FigString("5a")
			))
	)

	@Test
	fun testString() = test(
			"abc",
			FigList(mutableListOf(
					FigString("abc")
			))
	)

	@Test
	fun testNumber() = test(
			"1",
			FigList(mutableListOf(
					FigNumber(BigDecimal("1"))
			))
	)

	@Test
	fun listOfNumbers() = test(
			"[ 1 2.5 -3.5 -4 1E2 -1E2 -1.0E4 -2.3E-23 5E-2 5 ]",
			FigList(mutableListOf(
					FigNumber("1"),
					FigNumber("2.5"),
					FigNumber("-3.5"),
					FigNumber("-4"),
					FigNumber("1E2"),
					FigNumber("-1E2"),
					FigNumber("-1.0E4"),
					FigNumber("-2.3E-23"),
					FigNumber("5E-2"),
					FigNumber("5")
			))
	)

	@Test
	fun escapeQuotedString() = test(
			"\"a b\"",
			FigList(mutableListOf(
					FigString("a b")
			))
	)

	@Test
	fun escapeUnquotedString() = test(
			"""a\ b""",
			FigList(mutableListOf(
					FigString("a b")
			))
	)

	@Test
	fun listOfLiteralValues() = test(
			"""[ true null abc "A B" x ]""",
			FigList(mutableListOf(
					FigBoolean(true),
					null,
					FigString("abc"),
					FigString("A B"),
					FigString("x"),
			))
	)

	@Test
	fun listWithSublists() = test(
			""" [ [ null ] abc [ "hello world" whatever ] xx] """,
			FigList(mutableListOf(
					FigList(mutableListOf(null)),
					FigString("abc"),
					FigList(mutableListOf(
							FigString("hello world"),
							FigString("whatever")
					)),
					FigString("xx")
			))
	)

	@Test
	fun mapWithNullKeysAndMissingValues() = test(
			"""{ a:b c:d :e f }""",
			FigMap(map=mutableMapOf(
					"a" to FigString("b"),
					"c" to FigString("d"),
					null to FigString("e"),
					"f" to null
			))
	)

	@Test
	fun mapWithNullKeysAndNoValues() = test(
			"""{ a:b c:d :e f: }""",
			FigMap(map= mutableMapOf(
					"a" to FigString("b"),
					"c" to FigString("d"),
					null to FigString("e"),
					"f" to null
			) )
	)

	@Test
	fun namedMap() = test(
			"""{%abc a:b c:d :e f: }""",
			FigMap("abc", mutableMapOf(
					"a" to FigString("b"),
					"c" to FigString("d"),
					null to FigString("e"),
					"f" to null
			))
	)

	@Test
	fun mapSpaces() = test(
			"""{%abc a : b c : d : e f : }""",
			FigMap("abc", mutableMapOf(
					"a" to FigString("b"),
					"c" to FigString("d"),
					null to FigString("e"),
					"f" to null
			))
	)

	@Test
	fun namedMapWithSubObjects() = test(
			"""{%abc a:[b c] c:{%bar x:y} :e f: }""",
			FigMap("abc", mutableMapOf(
					"a" to FigList(mutableListOf(FigString("b"),FigString("c"))),
					"c" to FigMap("bar", mutableMapOf("x" to FigString("y"))),
					null to FigString("e"),
					"f" to null
			) )
	)

	@Test
	fun trailingEnds() = test(
			"""{%abc a:{ A:one B:two C:[ a ] } }""",
			FigMap("abc",mutableMapOf(
					"a" to FigMap(map= mutableMapOf(
							"A" to FigString("one"),
							"B" to FigString("two"),
							"C" to FigList(mutableListOf(
									FigString("a")
							))
					))
			))
	)

	@Test
	fun missingTrailingEnds() = test(
			"""{%abc a:{ A:one B:two C:[ a""",
			FigMap("abc",mutableMapOf(
					"a" to FigMap(map= mutableMapOf(
							"A" to FigString("one"),
							"B" to FigString("two"),
							"C" to FigList(mutableListOf(
									FigString("a")
							))
					))
			))
	)

	@Test
	fun implicitListStartMap() = test(
			"""{a:b} c""",
			FigList(mutableListOf(
					FigMap(map= mutableMapOf("a" to FigString("b"))),
					FigString("c")
			) )
	)

	@Test
	fun implicitListStartList() = test(
			"""[a b] c""",
			FigList(mutableListOf(
					FigList(mutableListOf(FigString("a"),FigString("b"))),
					FigString("c")
			))
	)

	@Test
	fun comments() = test("""<comm ment>{%abc<comment a>a:{<second comment>A:1 B:2 C:[ a ] } }<comment> <another comment>""")

}

