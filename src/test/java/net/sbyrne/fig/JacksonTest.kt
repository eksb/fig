package net.sbyrne.fig

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.core.Version
import com.fasterxml.jackson.databind.Module
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import org.junit.Test

class JacksonTest {

	val objectMapper = ObjectMapper()
			.registerModule(ModelModule)
			.registerKotlinModule()

	@Test
	fun test() {
		val jsonNode = FigParser
				.parse("""{ title:"my group" description:"blah blah blah" attendees:[ { @type:Robot id:1 } { @type:Person name:"John Doe" email:jdoe@example.com department:TECH } ]""")
				.toJsonNode()
		println( jsonNode )
		val group = objectMapper.reader().treeToValue(jsonNode,Group::class.java)
		println( group )
	}

	@Test
	fun testName() {
		val jsonNode = FigParser
				.parse("""{ title:"my group" description:"blah blah blah" attendees:[ {%Robot id:1} {%Person name:"John Doe" email:jdoe@example.com department:TECH} ]""")
				.toJsonNode()
		println( jsonNode )
		val group = objectMapper.reader().treeToValue(jsonNode,Group::class.java)
		println( group )
	}
}

data class Group (
		val title:String,
		val description:String,
		val attendees:List<Attendee>
)

interface Attendee

data class Robot(
		val id:Long
) : Attendee

data class Person(
		val name:String,
		val department:Department,
		val email:String
) : Attendee

enum class Department {
	TECH,
	RESEARCH
}

object ModelModule: Module() {
	override fun getModuleName():String = "Kotlunch Model Module"
	override fun version(): Version = Version.unknownVersion()

	override fun setupModule(context:SetupContext) {
		context.setMixInAnnotations( Attendee::class.java, AttendeeMixin::class.java )
		context.setMixInAnnotations( Robot::class.java, RobotMixin::class.java )
		context.setMixInAnnotations( Person::class.java, PersonMixin::class.java )
	}

}

@JsonTypeInfo(
		use = JsonTypeInfo.Id.NAME,
		include = JsonTypeInfo.As.PROPERTY,
		property = "@type"
)
@JsonSubTypes(
		JsonSubTypes.Type(value=Person::class,name="Person"),
		JsonSubTypes.Type(value=Robot::class,name="Robot")
)
abstract class AttendeeMixin

class RobotMixin {
	val id:Long? = null
}

class PersonMixin {
	val name:String? = null
	val department:Department? = null
	val email:String? = null
}
